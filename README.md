# yt-feeds.sh

YouTube video notification script

## About

This script is a wrapper for youtube-dl and keeps track of local YouTube subscriptions. It is intended to be run as a cron job to notify when new videos have been posted. No Google account required.

## Usage

1. Create a subscription list ~/.youtube-subs.txt with URLs for YouTube channels/users. One per line.
2. Edit and customise yt-feeds.sh (optional)
3. Run yt-feeds.sh

## Wrapper programs

A wrapper program can be provided using -w, for example torsocks or proxychains.