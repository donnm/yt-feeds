#!/bin/bash

# yt-feeds.sh - Notify of new YouTube videos using youtube-dl
#               without needed a Google account
# Author: Donn Morrison 2019 donn@tuta.io
# License: GPL 3.0

export SUBS=~/.youtube-subs.txt # Feeds (channel or user), one URL per line
export SEEN=~/.youtube-seen.txt # youtube-dl json output, one video per line
export TMP=~/.youtube-seen.tmp
export WRAPPER="torsocks" # Privacy wrapper for youtube-dl e.g., torsocks, proxychains, etc.
export WRAPPER_REINIT=""
export NUM_PROCS=2
export QUIET=0
export UPDATEONLY=0

trap ctrl_c INT

function ctrl_c()
{
    echo "Received CTRL-C, exiting..."
    exit 1
}

function cat-red()
{
    while read LINE; do    
        echo -e "\x1b[31m$LINE\x1b[0m"
    done
}

function update-feed()
{
    # { $WRAPPER youtube-dl -j --flat-playlist {} 2>&1 1>&3 3>&- | cat-red; } 3>&1 1>&2 | jq '{title,id}' 2>/dev/null | paste -d ' ' - - - - 1>> $TMP
    # Get the feed name
    FEEDNAME=$(2>/dev/null $WRAPPER youtube-dl "$1" -s | head -n2 | tail -n1 | grep -oP 'playlist: (\K.*)(?= - Videos)')
    if [ -z "$FEEDNAME" ]; then
        return 1
    fi

    if [ $QUIET -eq 0 ]; then
        echo "Updating feed $FEEDNAME $1"
    fi

    RES=$(2>/dev/null $WRAPPER youtube-dl -j --flat-playlist "$1")
    if [ $? -ne 0 ]; then
#        echo "Error downloading feed with youtube-dl $1" | cat-red 1>&2
        if [ "$WRAPPER" == "torsocks" ]; then
#            echo "Changing Tor circuit" | cat-red 1>&2
            echo -e "AUTHENTICATE\nSIGNAL NEWNYM\nQUIT" | nc localhost 9051 >/dev/null
        fi
        return 1
    fi

    TITLE_ID=$(echo "$RES" | 2>/dev/null jq '{title,id}')
    if [ $? -ne 0 ]; then
        echo "Error processing feed with jq $1" 1>&2
        return 1
    fi

    echo "$TITLE_ID" | paste -d ' ' - - - -  | sed "s/^{/{\"feedname\":\"$FEEDNAME\",/" > "$TMP.$(basename $(dirname $1))" # Eat newlines
    return 0
}

export -f update-feed
export -f cat-red

usage() {
    echo "Usage: $0 [options]" 1>&2
    echo 1>&2
    echo "options:" 1>&2
    echo -e "\t-j <num> number of concurrent threads (for GNU parallel)" 1>&2
    echo -e "\t-q quiet" 1>&2
    echo -e "\t-u update all feeds, don't announce new videos" 1>&2
    echo -e "\t-a <url> add/update a single feed, don't announce new videos" 1>&2
    echo -e "\t-w <cmd> wrap youtube-dl in another command, such as torsocks or proxychains" 1>&2
}

while getopts "uqj:a:w:" options; do
    case "${options}" in
        u)
            UPDATEONLY=1
            ;;
        q)
            QUIET=1
            ;;
        j)
            NUM_PROCS=${OPTARG}
            ;;
        w)
            WRAPPER=${OPTARG}
            ;;
        a)
            UPDATEONLY=1
            URL=${OPTARG}
            ;;
        :)
            usage
            exit 1
            ;;
        *)
            usage
            exit 1
            ;;
    esac
done

if ! command -v youtube-dl >/dev/null 2>&1; then
    echo "FATAL: youtube-dl missing, get it here: https://rg3.github.io/youtube-dl/download.html"
    exit 1
fi

# Remove old tmp files (do it at startup so we can debug a previous run)
if [ -e $TMP ]; then
    rm $TMP
fi

# Seen file initially empty (first time we're running)
if ! [ -e $SEEN ]; then
    touch $SEEN
fi

if [ -n "$URL" ]; then
    # Add/update a single URL
    grep "$URL" $SUBS 2>&1 >/dev/null
    if [ $? -eq 0 ]; then
        echo "Feed $URL already exists in $SUBS"
    else
        # Add to subscriptions
        echo "Adding feed $URL"
        echo "$URL" >> $SUBS
    fi
    # Update feed
    FEEDNAME=$(2>/dev/null $WRAPPER youtube-dl "$URL" -s | head -n2 | tail -n1 | grep -oP 'playlist: (\K.*)(?= - Videos)')
    if [ -z "$FEEDNAME" ]; then
        echo "Could not get feed name" 1>&2
        exit 1
    fi
    echo "Updating feed $FEEDNAME $URL"
    $WRAPPER youtube-dl -j --flat-playlist "$URL" >> $TMP
    echo "Synchronised $(wc -l < $TMP) videos"
else
    # Run with parallel if it exists
    # Ignore errors
    if command -v parallel >/dev/null 2>&1; then
        cat $SUBS | parallel -j $NUM_PROCS "update-feed"
    else
        echo "INFO: Install GNU parallel to update multiple feeds simultaneously."
        for feed in $(cat $SUBS); do
            if [ $QUIET -eq 0 ]; then echo "Updating feed $feed"; fi
            update-feed "$feed"
        done
    fi

    cat $TMP.* > $TMP
fi
# Sorting enables us to use diff (can't predict order feeds will be updated)
sort $TMP | uniq > $TMP.sorted
mv $TMP.sorted $TMP

if [ $UPDATEONLY -eq 1 ]; then
    # Just update the seen feeds
    cp $SEEN $SEEN.bak
    # Sort both TMP and SEEN, then remove duplicates
    sort $TMP $SEEN | uniq > $SEEN.sorted
    mv $SEEN.sorted $SEEN
else
    # We ignore all diff output with > in case some of the feed updates failed
    RES=$(diff $TMP $SEEN | grep '{' | grep -v '^>' | sed 's/^<\s\+//g' | jq -r .feedname,.title,.id | sed '0~3s#\(.*\)#http://youtube.com/embed/\1#' | paste - - -) # sed every third line to youtube id
    if [ -n "$RES" ]; then
        echo -ne '\007' # Bell for screen
#        echo "$(date +"%Y-%m-%d %H:%M:%S") New videos:"
        echo "$RES"

        cp $SEEN $SEEN.bak
        # Sort both TMP and SEEN, then remove duplicates
        sort $TMP $SEEN | uniq > $SEEN.sorted
        mv $SEEN.sorted $SEEN
    else
        if [ $QUIET -ne 1 ]; then
            echo "$(date +"%Y-%m-%d %H:%M:%S") No new videos."
        fi
    fi
fi

